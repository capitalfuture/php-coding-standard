# Sumofuturo coding standard

Coding standard rules to be used with [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) in **PHP projects**.

The ruleset extends from PSR-2, and includes:

* Do not allow array long syntax (`array(...)`)
* Make sure string concatenation operator is surrounded by spaces
* Do not allow superfluous whitespaces.
* Require `declare(strict_types=1);` at the beginning of every file
* Do not allow unused use statements
* Require alphabetically ordered use statements
* Require strict comparisons (`===` and `!==` instead of `==` and `!=`)
* Require trailing comma on every element of multi-line arrays
* Require no spaces between return type hint colon and function closing brace [`function (...): string`]
